<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <artifactId>root</artifactId>
  <groupId>com.secata.pom</groupId>
  <packaging>pom</packaging>
  <name>root</name>
  <version>3.52.0</version>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>17</maven.compiler.source>
    <maven.compiler.target>17</maven.compiler.target>
    <org.pitest.pitest-maven.version>1.15.1</org.pitest.pitest-maven.version>
    <org.apache.maven.plugins.maven-checkstyle-plugin.version>3.3.1</org.apache.maven.plugins.maven-checkstyle-plugin.version>
    <org.apache.maven.plugins.maven-surefire-plugin.version>3.1.2
    </org.apache.maven.plugins.maven-surefire-plugin.version>
    <org.apache.maven.plugins.maven-project-info-reports-plugin.version>3.4.5</org.apache.maven.plugins.maven-project-info-reports-plugin.version>
    <org.apache.maven.plugins.maven-compiler-plugin.version>3.11.0
    </org.apache.maven.plugins.maven-compiler-plugin.version>
    <org.apache.maven.plugins.maven-pmd-plugin.version>3.21.0
    </org.apache.maven.plugins.maven-pmd-plugin.version>
    <io.github.git-commit-id.git-commit-id-maven-plugin.version>7.0.0
    </io.github.git-commit-id.git-commit-id-maven-plugin.version>
    <org.apache.maven.plugins.maven-site-plugin.version>3.12.1
    </org.apache.maven.plugins.maven-site-plugin.version>
    <org.jacoco.jacoco-maven-plugin.version>0.8.11</org.jacoco.jacoco-maven-plugin.version>
    <org.apache.maven.plugins.maven-javadoc-plugin.version>3.6.0
    </org.apache.maven.plugins.maven-javadoc-plugin.version>
    <org.apache.maven.plugins.maven-source-plugin.version>3.3.0
    </org.apache.maven.plugins.maven-source-plugin.version>
    <spotless-maven-plugin.version>2.40.0</spotless-maven-plugin.version>
    <com.google.errorprone.version>2.23.0</com.google.errorprone.version>
    <org.owasp-dependency-check-plugin.version>8.4.0</org.owasp-dependency-check-plugin.version>
    <javadoc.additionalOptions/>
  </properties>

  <licenses>
    <license>
      <name>MIT</name>
      <url>https://opensource.org/licenses/MIT</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <repositories>
    <repository>
      <id>gitlab-secata</id>
      <url>https://gitlab.com/api/v4/groups/15475008/-/packages/maven</url>
    </repository>
  </repositories>
  <distributionManagement>
    <repository>
      <id>gitlab-project</id>
      <url>https://gitlab.com/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
    </repository>
  </distributionManagement>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>2.0.9</version>
      </dependency>
      <dependency>
        <groupId>ch.qos.logback</groupId>
        <artifactId>logback-classic</artifactId>
        <version>1.4.11</version>
      </dependency>
      <dependency>
        <groupId>org.assertj</groupId>
        <artifactId>assertj-core</artifactId>
        <version>3.24.2</version>
      </dependency>
      <dependency>
        <groupId>nl.jqno.equalsverifier</groupId>
        <artifactId>equalsverifier</artifactId>
        <version>3.15.2</version>
      </dependency>
      <dependency>
        <groupId>net.bytebuddy</groupId>
        <artifactId>byte-buddy</artifactId>
        <version>1.14.9</version>
      </dependency>
      <dependency>
        <groupId>org.bouncycastle</groupId>
        <artifactId>bcpkix-jdk18on</artifactId>
        <version>1.76</version>
      </dependency>
      <dependency>
        <groupId>org.bouncycastle</groupId>
        <artifactId>bcprov-jdk18on</artifactId>
        <version>1.76</version>
      </dependency>
      <dependency>
        <groupId>org.bouncycastle</groupId>
        <artifactId>bctls-jdk18on</artifactId>
        <version>1.76</version>
      </dependency>
      <dependency>
        <groupId>org.web3j</groupId>
        <artifactId>core</artifactId>
        <version>5.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.google.errorprone</groupId>
        <artifactId>error_prone_annotations</artifactId>
        <version>${com.google.errorprone.version}</version>
      </dependency>
      <dependency>
        <groupId>net.java.dev.jna</groupId>
        <artifactId>jna</artifactId>
        <version>5.13.0</version>
      </dependency>
      <dependency>
        <groupId>org.openjdk.jmh</groupId>
        <artifactId>jmh-core</artifactId>
        <version>1.37</version>
      </dependency>
      <dependency>
        <groupId>org.openjdk.jmh</groupId>
        <artifactId>jmh-generator-annprocess</artifactId>
        <version>1.37</version>
      </dependency>
      <!-- BOM imports -->
      <dependency>
        <groupId>jakarta.platform</groupId>
        <artifactId>jakarta.jakartaee-bom</artifactId>
        <version>10.0.0</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <!-- Include newer xml.bind-api since it is used from jaxb-bom -->
      <dependency>
        <groupId>jakarta.xml.bind</groupId>
        <artifactId>jakarta.xml.bind-api</artifactId>
        <version>4.0.1</version>
      </dependency>
      <dependency>
        <groupId>jakarta.activation</groupId>
        <artifactId>jakarta.activation-api</artifactId>
        <version>2.1.2</version>
      </dependency>
      <dependency>
        <groupId>com.fasterxml.jackson</groupId>
        <artifactId>jackson-bom</artifactId>
        <version>2.15.3</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>org.glassfish.jersey</groupId>
        <artifactId>jersey-bom</artifactId>
        <version>3.1.3</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>org.glassfish.jaxb</groupId>
        <artifactId>jaxb-bom</artifactId>
        <version>4.0.4</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>com.github.docker-java</groupId>
        <artifactId>docker-java-bom</artifactId>
        <version>3.3.3</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>org.junit</groupId>
        <artifactId>junit-bom</artifactId>
        <version>5.10.0</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-bom</artifactId>
        <version>5.6.0</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <!-- Don't provide versions in this section. -->
  <!-- Use dependencyManagement instead to ensure transitive dependencies version -->
  <!-- and avoid exclusions -->
  <dependencies>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.assertj</groupId>
      <artifactId>assertj-core</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>nl.jqno.equalsverifier</groupId>
      <artifactId>equalsverifier</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-compiler-plugin.version}</version>
        <configuration>
          <showWarnings>true</showWarnings>
          <compilerArgs>
            <arg>-Xlint:all</arg>
            <arg>-Xlint:-processing</arg>
            <arg>-Xlint:-options</arg>
            <arg>-Werror</arg>
          </compilerArgs>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>${org.jacoco.jacoco-maven-plugin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
          </execution>
          <execution>
            <id>report</id>
            <phase>test</phase>
            <goals>
              <goal>report</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-checkstyle-plugin.version}</version>
        <configuration>
          <includeTestSourceDirectory>true</includeTestSourceDirectory>
          <linkXRef>false</linkXRef>
          <violationSeverity>warning</violationSeverity>
          <configLocation>google_checks.xml</configLocation>
          <sourceDirectories>
            <sourceDirectory>${project.build.sourceDirectory}</sourceDirectory>
            <sourceDirectory>${project.build.testSourceDirectory}</sourceDirectory>
          </sourceDirectories>
        </configuration>
        <dependencies>
          <dependency>
            <groupId>com.puppycrawl.tools</groupId>
            <artifactId>checkstyle</artifactId>
            <version>10.12.4</version>
          </dependency>
        </dependencies>
      </plugin>
      <plugin>
        <groupId>org.pitest</groupId>
        <artifactId>pitest-maven</artifactId>
        <version>${org.pitest.pitest-maven.version}</version>
        <configuration>
          <timeoutConstant>15000</timeoutConstant>
          <threads>4</threads>
          <timestampedReports>false</timestampedReports>
          <failWhenNoMutations>false</failWhenNoMutations>
          <outputFormats>
            <outputFormat>HTML</outputFormat>
            <outputFormat>CSV</outputFormat>
          </outputFormats>
          <!-- No op arg line to suppress default that accidentally includes the JaCoCo agent
               See https://github.com/hcoles/pitest/issues/1070 -->
          <argLine>-Dfile.encoding=UTF-8</argLine>
        </configuration>
        <dependencies>
          <dependency>
            <groupId>org.pitest</groupId>
            <artifactId>pitest-junit5-plugin</artifactId>
            <version>1.2.0</version>
          </dependency>
        </dependencies>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-surefire-plugin.version}</version>
        <configuration>
          <trimStackTrace>false</trimStackTrace>
          <parallel>classesAndMethods</parallel>
          <threadCount>10</threadCount>
          <perCoreThreadCount>false</perCoreThreadCount>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-site-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-site-plugin.version}</version>
      </plugin>
      <plugin>
        <groupId>io.github.git-commit-id</groupId>
        <artifactId>git-commit-id-maven-plugin</artifactId>
        <version>${io.github.git-commit-id.git-commit-id-maven-plugin.version}</version>
        <executions>
          <execution>
            <id>get-the-git-infos</id>
            <goals>
              <goal>revision</goal>
            </goals>
          </execution>
          <execution>
            <id>validate-the-git-infos</id>
            <goals>
              <goal>validateRevision</goal>
            </goals>
            <phase>package</phase>
          </execution>
        </executions>
        <configuration>
          <prefix>git</prefix>
          <dateFormat>yyyy-MM-dd'T'HH:mm:ssZ</dateFormat>
          <dateFormatTimeZone>${user.timezone}</dateFormatTimeZone>
          <verbose>false</verbose>
          <generateGitPropertiesFile>true</generateGitPropertiesFile>
          <generateGitPropertiesFilename>
            ${project.build.outputDirectory}/${project.groupId}.${project.artifactId}-git.properties
          </generateGitPropertiesFilename>
          <format>properties</format>
          <failOnNoGitDirectory>true</failOnNoGitDirectory>
          <includeOnlyProperties>
            <includeOnlyProperty>git.build.time</includeOnlyProperty>
            <includeOnlyProperty>git.build.version</includeOnlyProperty>
            <includeOnlyProperty>git.commit.id.full</includeOnlyProperty>
            <includeOnlyProperty>git.commit.id.abbrev</includeOnlyProperty>
          </includeOnlyProperties>
          <useNativeGit>false</useNativeGit>
          <abbrevLength>7</abbrevLength>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-source-plugin.version}</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-pmd-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-pmd-plugin.version}</version>
        <configuration>
          <format>csv</format>
          <includeTests>true</includeTests>
          <linkXRef>false</linkXRef>
          <skipEmptyReport>false</skipEmptyReport>
          <compileSourceRoots>
            <compileSourceRoot>${project.build.sourceDirectory}</compileSourceRoot>
          </compileSourceRoots>
          <testSourceRoots>
            <testSourceRoot>${project.build.testSourceDirectory}</testSourceRoot>
          </testSourceRoots>
        </configuration>
      </plugin>
      <plugin>
        <groupId>com.diffplug.spotless</groupId>
        <artifactId>spotless-maven-plugin</artifactId>
        <version>${spotless-maven-plugin.version}</version>
        <configuration>
          <formats>
            <format>
              <includes>
                <include>*.html</include>
                <include>*.json</include>
                <include>*.properties</include>
                <include>*.xml</include>
                <include>*.yml</include>
                <include>.gitignore</include>
              </includes>
              <trimTrailingWhitespace/>
              <endWithNewline/>
              <indent>
                <spaces>true</spaces>
                <spacesPerTab>2</spacesPerTab>
              </indent>
            </format>
          </formats>
          <java>
            <googleJavaFormat>
              <version>1.16.0</version>
              <style>GOOGLE</style>
              <reflowLongStrings>true</reflowLongStrings>
            </googleJavaFormat>
          </java>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-javadoc-plugin.version}</version>
        <configuration>
          <quiet>true</quiet>
          <failOnWarnings>true</failOnWarnings>
          <additionalOptions>${javadoc.additionalOptions}</additionalOptions>
          <tags>
            <tag>
              <name>pre</name>
              <placement>a</placement>
              <head>Preconditions</head>
            </tag>
            <tag>
              <name>post</name>
              <placement>a</placement>
              <head>Postconditions</head>
            </tag>
          </tags>
        </configuration>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.owasp</groupId>
        <artifactId>dependency-check-maven</artifactId>
        <version>${org.owasp-dependency-check-plugin.version}</version>
        <configuration>
          <!-- https://nvd.nist.gov/vuln-metrics/cvss#, 9-10 means critical -->
          <failBuildOnCVSS>9</failBuildOnCVSS>
          <!-- Disable oss index analyzer due to rate limit errors in pipeline -->
          <ossindexAnalyzerEnabled>false</ossindexAnalyzerEnabled>
          <ossIndexWarnOnlyOnRemoteErrors>true</ossIndexWarnOnlyOnRemoteErrors>
          <nuspecAnalyzerEnabled>false</nuspecAnalyzerEnabled>
          <assemblyAnalyzerEnabled>false</assemblyAnalyzerEnabled>
          <msbuildAnalyzerEnabled>false</msbuildAnalyzerEnabled>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-javadoc-plugin.version}</version>
        <configuration>
          <quiet>true</quiet>
          <detectLinks/>
          <additionalOptions>${javadoc.additionalOptions}</additionalOptions>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>${org.jacoco.jacoco-maven-plugin.version}</version>
        <reportSets>
          <reportSet>
            <reports>
              <report>report</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-pmd-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-pmd-plugin.version}</version>
        <configuration>
          <format>csv</format>
          <includeTests>true</includeTests>
          <linkXRef>false</linkXRef>
          <skipEmptyReport>false</skipEmptyReport>
          <compileSourceRoots>
            <compileSourceRoot>${project.build.sourceDirectory}</compileSourceRoot>
          </compileSourceRoots>
          <testSourceRoots>
            <testSourceRoot>${project.build.testSourceDirectory}</testSourceRoot>
          </testSourceRoots>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-project-info-reports-plugin.version}</version>
        <reportSets>
          <reportSet>
            <configuration>
              <skip>true</skip>
            </configuration>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>${org.apache.maven.plugins.maven-checkstyle-plugin.version}</version>
        <configuration>
          <includeTestSourceDirectory>true</includeTestSourceDirectory>
          <linkXRef>false</linkXRef>
          <configLocation>google_checks.xml</configLocation>
          <sourceDirectories>
            <sourceDirectory>${project.build.sourceDirectory}</sourceDirectory>
            <sourceDirectory>${project.build.testSourceDirectory}</sourceDirectory>
          </sourceDirectories>
        </configuration>
      </plugin>
    </plugins>
  </reporting>

  <profiles>
    <profile>
      <id>errorprone</id>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>${org.apache.maven.plugins.maven-compiler-plugin.version}</version>
            <configuration>
              <showWarnings>true</showWarnings>
              <fork>true</fork>
              <compilerArgs>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.api=ALL-UNNAMED</arg>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.file=ALL-UNNAMED</arg>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.main=ALL-UNNAMED</arg>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.model=ALL-UNNAMED</arg>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.parser=ALL-UNNAMED</arg>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.processing=ALL-UNNAMED</arg>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.tree=ALL-UNNAMED</arg>
                <arg>-J--add-exports=jdk.compiler/com.sun.tools.javac.util=ALL-UNNAMED</arg>
                <arg>-J--add-opens=jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED</arg>
                <arg>-J--add-opens=jdk.compiler/com.sun.tools.javac.comp=ALL-UNNAMED</arg>
                <arg>-Xlint:-path</arg>
                <arg>-Xlint:all</arg>
                <arg>-Xlint:-processing</arg>
                <arg>-Werror</arg>
                <arg>-XDcompilePolicy=simple</arg>
                <arg>-Xplugin:ErrorProne -XepExcludedPaths:.*/target/.* -Xep:InvalidBlockTag:OFF
                </arg>
              </compilerArgs>
              <annotationProcessorPaths>
                <path>
                  <groupId>com.google.errorprone</groupId>
                  <artifactId>error_prone_core</artifactId>
                  <version>${com.google.errorprone.version}</version>
                </path>
              </annotationProcessorPaths>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
